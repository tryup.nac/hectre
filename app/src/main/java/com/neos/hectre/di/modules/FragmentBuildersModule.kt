package com.neos.hectre.di.modules


import com.neos.hectre.presenter.view.fragment.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 *created by Neos on 11-March-21.
 **/
@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment
}

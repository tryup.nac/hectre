package com.neos.hectre.di.modules

import dagger.Module

/**
 *created by Neos on 11-March-21.
 **/
@Module(
    includes = [
        ContextModule::class,
        DataSourceModule::class,
        ViewModelModule::class
    ]
)

class AppModule
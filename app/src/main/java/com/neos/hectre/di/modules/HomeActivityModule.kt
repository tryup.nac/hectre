package com.neos.hectre.di.modules


import com.neos.hectre.presenter.view.activity.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 *created by Neos on 11-March-21.
 **/
@Suppress("unused")
@Module
abstract class HomeActivityModule {
    @ContributesAndroidInjector(
        modules = [
            FragmentBuildersModule::class
        ]
    )

    abstract fun contributeMainActivity(): HomeActivity
}

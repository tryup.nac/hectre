package com.neos.hectre.di.modules

import android.content.Context
import androidx.viewbinding.BuildConfig
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.neos.hectre.App
import com.neos.hectre.common.Constants
import com.neos.hectre.data.remote.APIs
import com.neos.hectre.data.remote.dto.JobDto
import com.neos.hectre.data.remote.repository.JobRepositoryImpl
import com.neos.hectre.domain.repository.JobRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.InputStream
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton


/**
 *created by Neos on 11-March-21.
 **/
@Module
object DataSourceModule {
    private val loggingInterceptor =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

    @Singleton
    @Provides
    @Named("Server")
    fun providesServerAPI(): APIs {
        val okHttpBuilder = OkHttpClient.Builder()
            .callTimeout(Constants.TIME_OUT, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) okHttpBuilder.addInterceptor(loggingInterceptor)
        okHttpBuilder.addNetworkInterceptor {
            val request: Request = it.request()
            val newRequest: Request =
                request.newBuilder().addHeader(Constants.HEADER_AUTHORIZATION, Constants.API_KEY)
                    .build()
            it.proceed(newRequest)
        }

        val builder = Retrofit.Builder()
        val retrofit = builder.baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpBuilder.build()).build()
        return retrofit.create(APIs::class.java)
    }

    @Singleton
    @Provides
    @Named("Mock")
    fun providesMockAPI(context: Context): APIs {
        return object : APIs {
            override suspend fun listJobs(): List<JobDto> {
                try {
                    val inputStream: InputStream = context.assets.open("api.json")
                    val json = inputStream.bufferedReader().use { it.readText() }
                    val listType: Type = object : TypeToken<List<JobDto>>() {}.type
                    return Gson().fromJson(json, listType)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
                return ArrayList()
            }
        }
    }

    @Provides
    @Singleton
    fun provideJobsRepository(@Named("Mock") api: APIs): JobRepository {
        return JobRepositoryImpl(api)
    }
}
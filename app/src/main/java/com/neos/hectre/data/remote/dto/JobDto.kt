package com.neos.hectre.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.neos.hectre.domain.model.Cell
import com.neos.hectre.domain.model.Job
import java.io.Serializable

/**
 *created by Neos on 11-March-21.
 **/
class JobDto : Serializable {
    fun toJob(): Job {
        val listStaffJob = listStaffJob?.map { it.toStaffJob() }
        val cells =
            listCell?.map {
                Cell(
                    it.cellId,
                    it.value,
                    it.state,
                    it.sharedJob?.map { it2 -> it2.toSharedJob() })
            }
        cells?.sortedBy { it.cellId }
        return Job(id, jobName, orchard, block, listStaffJob, cells)
    }

    @SerializedName("id")
    var id: String? = null

    @SerializedName("jobName")
    val jobName: String? = null

    @SerializedName("orchard")
    val orchard: String? = null

    @SerializedName("block")
    val block: String? = null

    @SerializedName("staffJob")
    val listStaffJob: List<StaffJobDto>? = null

    @SerializedName("cell")
    val listCell: List<CelDto>? = null
}

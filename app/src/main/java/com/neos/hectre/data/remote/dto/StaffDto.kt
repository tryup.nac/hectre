package com.neos.hectre.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.neos.hectre.domain.model.Staff
import java.io.Serializable

/**
 *created by Neos on 11-March-21.
 **/
class StaffDto : Serializable {
    fun toStaff(): Staff {
        return Staff(id, firstName, lastName, wages)
    }

    @SerializedName("id")
    var id: String? = null

    @SerializedName("firstName")
    val firstName: String? = null

    @SerializedName("lastName")
    val lastName: String? = null

    @SerializedName("wages")
    val wages: Int = 0
}

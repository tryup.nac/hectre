package com.neos.hectre.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.neos.hectre.domain.model.Cell
import com.neos.hectre.domain.model.StaffJob
import java.io.Serializable

/**
 *created by Neos on 11-March-21.
 **/
class StaffJobDto : Serializable {
    @SerializedName("staff")
    var staff: StaffDto? = null

    @SerializedName("rateType")
    val rateType: String? = null

    @SerializedName("price")
    val price: Int? = null

    @SerializedName("cell")
    val listCell: List<CelDto>? = null

    fun toStaffJob(): StaffJob {
        val cells = listCell?.map {
            Cell(
                it.cellId,
                it.value,
                it.state,
                it.sharedJob?.map { it2 -> it2.toSharedJob() })
        }
        cells?.sortedBy { it.cellId }
        return StaffJob(staff?.toStaff(), rateType, price, cells)
    }
}
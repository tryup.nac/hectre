package com.neos.hectre.data.remote.repository

import com.neos.hectre.data.remote.APIs
import com.neos.hectre.data.remote.dto.JobDto
import com.neos.hectre.domain.repository.JobRepository
import javax.inject.Inject

/**
 *created by Neos on 11-March-21.
 **/
class JobRepositoryImpl @Inject constructor(private val api: APIs) : JobRepository {
    override suspend fun listJobs(): List<JobDto> {
        return api.listJobs()
    }
}
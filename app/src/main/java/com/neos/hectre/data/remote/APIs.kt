package com.neos.hectre.data.remote

import com.neos.hectre.data.remote.dto.JobDto
import retrofit2.http.GET

/**
 *created by Neos on 11-March-21.
 **/
interface APIs {
    //1. Listing jobs
    @GET("jobs")
    suspend fun listJobs(): List<JobDto>
}
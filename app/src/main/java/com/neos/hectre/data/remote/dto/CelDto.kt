package com.neos.hectre.data.remote.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 *created by Neos on 11-March-21.
 **/
class CelDto : Serializable {
    @SerializedName("cellId")
    var cellId: String? = null

    @SerializedName("value")
    var value: Int = 0

    @SerializedName("state")
    var state: Int = 0

    @SerializedName("sharedJob")
    val sharedJob: List<SharedJobDto>? = null
}
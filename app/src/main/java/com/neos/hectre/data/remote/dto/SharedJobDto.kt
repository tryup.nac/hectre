package com.neos.hectre.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.neos.hectre.domain.model.SharedJob
import com.neos.hectre.domain.model.Staff
import java.io.Serializable

/**
 *created by Neos on 11-March-21.
 **/
class SharedJobDto : Serializable {
    fun toSharedJob(): SharedJob {
        return SharedJob(staff?.toStaff(), sharedValue)
    }

    @SerializedName("staff")
    var staff: StaffDto? = null

    @SerializedName("sharedValue")
    val sharedValue: Int = 0
}

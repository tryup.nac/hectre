package com.neos.hectre.domain.model

/**
 *created by Neos on 11-March-21.
 **/
data class StaffJob(
    var staff: Staff?,
    var rateType: String?,
    var price: Int?,
    var cells: List<Cell>?
)
package com.neos.hectre.domain.repository

import com.neos.hectre.data.remote.dto.JobDto

/**
 *created by Neos on 11-March-21.
 **/
interface JobRepository {
    suspend fun listJobs(): List<JobDto>
}
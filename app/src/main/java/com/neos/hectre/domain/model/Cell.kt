package com.neos.hectre.domain.model

import com.neos.hectre.data.remote.dto.SharedJobDto

/**
 *created by Neos on 11-March-21.
 **/
data class Cell(
    val cellId: String? = null,
    var value: Int? = 0,
    var state: Int? = 0,
    val sharedJob: List<SharedJob>? = null
)
package com.neos.hectre.domain.model

/**
 *created by Neos on 11-March-21.
 **/
data class Staff(
    var id: String?,
    var firstName: String?,
    var lastName: String?,
    var wages: Int?,
) {
    override fun equals(other: Any?): Boolean {
        return id == (other as Staff).id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
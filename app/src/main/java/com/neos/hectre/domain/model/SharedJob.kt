package com.neos.hectre.domain.model

/**
 *created by Neos on 11-March-21.
 **/
data class SharedJob(
    var staff: Staff?,
    var sharedValue: Int
)
package com.neos.hectre.domain.model

/**
 *created by Neos on 11-March-21.
 **/
data class Job(
    var id: String?,
    val jobName: String?,
    val orchard: String?,
    val block: String?,
    val listStaffJob: List<StaffJob>?,
    val listCell: List<Cell>?
)
package com.neos.hectre.domain.usecase

import com.neos.hectre.common.Resource
import com.neos.hectre.domain.model.Job
import com.neos.hectre.domain.repository.JobRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

/**
 *created by Neos on 11-March-21.
 **/
class JobUseCase @Inject constructor(
    private val jobRepository: JobRepository
) {

    fun listJobs(): Flow<Resource<List<Job>>> = flow {
        try {
            emit(Resource.Loading())
            val rs = jobRepository.listJobs()
            val listJob = rs.map { it.toJob() }
            emit(Resource.Success(listJob))
        } catch (e: HttpException) {
            emit(Resource.Failed(e, e.code()))
        } catch (e: IOException) {
            emit(Resource.Exception(e))
        }
    }
}
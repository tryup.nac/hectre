package com.neos.hectre.presenter.view.fragment.screen

import com.neos.hectre.domain.model.Job

/**
 *created by Neos on 11-March-21.
 **/
class ListedJobsState(
    code: Int? = -1,
    key: Int? = -1,
    data: List<Job>? = null,
    msg: String? = null
) : UIState<List<Job>>() {
    init {
        this.code = code
        this.key = key
        this.data = data
        this.msg = msg
    }
}
package com.neos.hectre.presenter.view.fragment.screen

/**
 *created by Neos on 11-March-21.
 **/
open class UIState<T> {
    var code: Int? = 0
    var key: Int? = 0
    var data: T? = null
    var msg: String? = null
}
package com.neos.hectre.presenter.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.neos.hectre.R
import com.neos.hectre.common.Constants
import com.neos.hectre.domain.model.Cell
import com.neos.hectre.domain.model.Job

class CellAdapter(
    private val listCell: ArrayList<*>?,
    private val context: Context?,
    private val event: (Constants.Action, Cell) -> Boolean
) :
    RecyclerView.Adapter<CellAdapter.JobHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_cell, parent, false)
        return JobHolder(v)
    }

    @SuppressLint("UseCompatTextViewDrawableApis")
    override fun onBindViewHolder(holder: JobHolder, position: Int) {
        val item = listCell?.get(position) as Cell
        holder.tvCell.text = item.cellId
        holder.lnCell.tag = item
        holder.lnCell.backgroundTintList =
            context?.getColorStateList(if (item.state == 0) R.color.gray_light else R.color.blue_200)
        holder.tvCell.setTextColor(if (item.state == 0) Color.BLACK else Color.WHITE)
        holder.vDot.visibility =
            if (item.state == 1 && event(
                    Constants.Action.DO_LONG_CLICK,
                    item
                )
            ) View.VISIBLE else View.GONE
    }

    override fun getItemCount(): Int {
        return listCell?.size ?: 0
    }

    @SuppressLint("NotifyDataSetChanged")
    inner class JobHolder(v: View) : RecyclerView.ViewHolder(v) {
        val lnCell: FrameLayout = v.findViewById(R.id.fr_cell)
        val tvCell: TextView = v.findViewById(R.id.tv_id)
        val vDot: View = v.findViewById(R.id.v_dot)

        init {
            tvCell.setOnClickListener {
                val cell = listCell?.find { (it as Cell).cellId == (lnCell.tag as Cell).cellId }
                (cell as Cell).state = if (cell.state == 0) 1 else 0
                event(Constants.Action.DO_CLICK, cell)
                notifyDataSetChanged()
            }
        }
    }
}
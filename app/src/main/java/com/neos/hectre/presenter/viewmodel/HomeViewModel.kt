package com.neos.hectre.presenter.viewmodel

import androidx.lifecycle.viewModelScope
import com.neos.hectre.domain.usecase.JobUseCase
import com.neos.hectre.presenter.view.fragment.screen.ListedJobsState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

/**
 *created by Neos on 11-March-21.
 **/
class HomeViewModel @Inject constructor(
    private val useCase: JobUseCase,
) : BaseViewModel<ListedJobsState>() {

    fun listJobs() {
        useCase.listJobs()
            .onEach { handleState(dataState, it) }
            .launchIn(viewModelScope)
    }
}
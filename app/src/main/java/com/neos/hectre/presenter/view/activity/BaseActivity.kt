package com.neos.hectre.presenter.view.activity

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity<T : ViewBinding?, M : ViewModel?> : DaggerAppCompatActivity(),
    View.OnClickListener {
    protected var binding: T? = null
    protected var viewModel: M? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = initViewBinding()
        viewModel = ViewModelProvider(this).get(initViewModel())
        setContentView(binding!!.root)
        initViews()
    }

    protected abstract fun initViews()
    protected abstract fun initViewBinding(): T
    protected abstract fun initViewModel(): Class<M>

    override fun onClick(v: View) {
        //do nothing
    }

    protected fun notify(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    protected fun notify(msg: Int) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
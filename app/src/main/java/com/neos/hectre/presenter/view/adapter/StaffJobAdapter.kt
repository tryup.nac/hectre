package com.neos.hectre.presenter.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.neos.hectre.R
import com.neos.hectre.common.Constants
import com.neos.hectre.common.fadeIn
import com.neos.hectre.domain.model.Cell
import com.neos.hectre.domain.model.Job
import com.neos.hectre.domain.model.StaffJob
import kotlin.random.Random.Default.nextInt

class StaffJobAdapter(
    private val job: Job,
    private val context: Context?,
    private val event: (Constants.Action, StaffJob) -> Unit
) :
    RecyclerView.Adapter<StaffJobAdapter.JobHolder>() {
    companion object {
        val COLOR_BG = mutableListOf(
            R.color.yellowDark2, R.color.orangeDark2, R.color.greenDark2, R.color.blueDark2,
            R.color.purpleDark2,
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_staff_job, parent, false)
        return JobHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: JobHolder, position: Int) {
        val item = job.listStaffJob?.get(position) as StaffJob
        holder.tvBlock.text = job.block
        holder.tvOrchard.text = job.orchard

        holder.tvPrefix.backgroundTintList =
            context?.getColorStateList(COLOR_BG[nextInt(COLOR_BG.size)])
        holder.tvPrefix.text =
            "${(item.staff?.firstName ?: " ")[0]}${(item.staff?.lastName ?: " ")[0]}".trim()

        holder.tvName.tag = item
        holder.tvName.text = "${item.staff?.firstName} ${item.staff?.lastName ?: ""}"
        holder.tvTitleWages.text = context?.getString(R.string.txt_title_wages, job.jobName)
        holder.edtRate.setText(item.price?.toString())
        if (item.rateType == Constants.PRICE_RATE) {
            holder.showPriceRate()
        } else {
            holder.showWages()
        }
        holder.rvCell.adapter = CellAdapter(ArrayList(item.cells!!), context) { key, cell ->
            if (key == Constants.Action.DO_CLICK) {
                initListTreeCell(holder.lnTreeCell, item.cells)
                true
            } else {
                job.listCell?.find { it.cellId == cell.cellId }?.sharedJob != null
            }

        }
        initListTreeCell(holder.lnTreeCell, item.cells)
    }

    @SuppressLint("SetTextI18n")
    private fun initListTreeCell(lnTreeCell: LinearLayout, cells: List<Cell>?) {
        lnTreeCell.removeAllViews()
        if (cells == null || cells.isEmpty()) return
        for (cell in cells) {
            if (cell.state == 0) continue
            val v = LayoutInflater.from(context).inflate(R.layout.item_tree_cell, null)
            val tvTreeName = v.findViewById<TextView>(R.id.tv_tree_name)
            val edtCell = v.findViewById<EditText>(R.id.edt_value)
            val tvName = v.findViewById<TextView>(R.id.tv_name)
            val tvTotal = v.findViewById<TextView>(R.id.tv_total)

            edtCell.setText(cell.value?.toString())
            tvTotal.text = "/ ${job.listCell?.find { it.cellId == cell.cellId }?.value.toString()}"
            tvTreeName.text = "${context?.getText(R.string.txt_tree_for_now)} ${cell.cellId}"

            tvName.text = job.listCell?.find {
                it.cellId == cell.cellId
            }?.sharedJob?.joinToString {
                "${(it.staff?.firstName ?: " ")}${(it.staff?.lastName ?: " ")}".trim() + "(${it.sharedValue})"
            }
            lnTreeCell.addView(v)
        }
    }

    override fun getItemCount(): Int {
        return job.listStaffJob?.size ?: 0
    }

    @SuppressLint("NotifyDataSetChanged")
    inner class JobHolder(v: View) : RecyclerView.ViewHolder(v) {
        private val trPrice: TableRow = v.findViewById(R.id.tr_price)
        val tvTitleWages: TextView = v.findViewById(R.id.tv_title_wages)
        private val tvWages: TextView = v.findViewById(R.id.tv_wages)
        private val tvPriceRate: TextView = v.findViewById(R.id.tv_price_rate)
        private val tvApply: TextView = v.findViewById(R.id.tv_apply)

        val edtRate: EditText = v.findViewById(R.id.edt_rate)
        val tvPrefix: TextView = v.findViewById(R.id.tv_prefix)
        val tvName: TextView = v.findViewById(R.id.tv_name)
        val tvOrchard: TextView = v.findViewById(R.id.tv_orchard)
        val tvBlock: TextView = v.findViewById(R.id.tv_block)
        val rvCell: RecyclerView = v.findViewById(R.id.rv_cell)
        val lnTreeCell: LinearLayout = v.findViewById(R.id.ln_tree_cell)

        init {
            tvPriceRate.setOnClickListener {
                showPriceRate()
                job.listStaffJob?.find { it.staff?.id == (tvName.tag as StaffJob).staff?.id }?.rateType =
                    Constants.PRICE_RATE
            }
            tvWages.setOnClickListener {
                showWages()
                job.listStaffJob?.find { it.staff?.id == (tvName.tag as StaffJob).staff?.id }?.rateType =
                    Constants.WAGES
            }
            tvApply.setOnClickListener {
                it.fadeIn(context!!)
                job.listStaffJob?.forEach { it2 ->
                    if (it2.rateType == Constants.PRICE_RATE) {
                        it2.price = edtRate.text?.toString()?.toInt()
                    }
                    notifyDataSetChanged()
                }
            }
        }

        fun showPriceRate() {
            trPrice.visibility = View.VISIBLE
            tvTitleWages.visibility = View.GONE
            tvPriceRate.backgroundTintList = context?.getColorStateList(R.color.orangeDark1)
            tvWages.backgroundTintList = context?.getColorStateList(R.color.gray_mid)
        }

        fun showWages() {
            trPrice.visibility = View.GONE
            tvTitleWages.visibility = View.VISIBLE
            tvPriceRate.backgroundTintList = context?.getColorStateList(R.color.gray_mid)
            tvWages.backgroundTintList = context?.getColorStateList(R.color.orangeDark1)
        }
    }
}
package com.neos.hectre.presenter.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.neos.hectre.R
import com.neos.hectre.common.Constants
import com.neos.hectre.common.fadeIn
import com.neos.hectre.domain.model.Job

class JobsAdapter(
    private val listJob: ArrayList<*>?,
    private val context: Context?,
    private val event: (Constants.Action, Job) -> Unit
) :
    RecyclerView.Adapter<JobsAdapter.JobHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_job, parent, false)
        return JobHolder(v)
    }

    override fun onBindViewHolder(holder: JobHolder, position: Int) {
        val item = listJob?.get(position) as Job
        holder.tvJobName.text = item.jobName
        holder.tvJobName.tag = item
        holder.rvJob.adapter = StaffJobAdapter(item, context) { _, _ -> }
    }

    override fun getItemCount(): Int {
        return listJob?.size ?: 0
    }

    @SuppressLint("NotifyDataSetChanged")
    inner class JobHolder(v: View) : RecyclerView.ViewHolder(v) {
        private val tvAddMaxTree: TextView = v.findViewById(R.id.tv_add_max_tree)
        val tvJobName: TextView = v.findViewById(R.id.tv_job_name)
        val rvJob: RecyclerView = v.findViewById(R.id.rv_job)

        init {
            tvAddMaxTree.setOnClickListener { it ->
                it.fadeIn(context!!)
                var job = tvJobName.tag as Job
                job = listJob?.find { (it as Job).id == job.id } as Job
                for (cell in job.listCell!!) {
                    val occupiedTotal = cell.sharedJob?.sumOf {
                        it.sharedValue
                    } ?: 0
                    val activeStaff = job.listStaffJob?.count {
                        it.cells?.find { it2 -> it2.cellId == cell.cellId }?.state == 1
                    }
                    if (activeStaff == 0) continue
                    val restValue = ((cell.value ?: 0) - occupiedTotal)
                    val equalValue = restValue / (activeStaff ?: 1)
                    var odd = restValue % 2
                    job.listStaffJob?.forEach {
                        val itemCell = it.cells?.find { it2 ->
                            it2.cellId == cell.cellId && it2.state == 1
                        }
                        if (itemCell != null) {
                            itemCell.value = equalValue + odd
                            odd = 0
                        }
                    }
                }
                notifyDataSetChanged()
            }
        }
    }
}
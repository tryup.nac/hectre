package com.neos.hectre.presenter.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.neos.hectre.R
import com.neos.hectre.common.Constants
import com.neos.hectre.common.notify
import com.neos.hectre.presenter.view.activity.HomeActivity
import com.neos.hectre.presenter.view.fragment.screen.UIState
import com.neos.hectre.presenter.view.widget.ProgressLoading
import com.neos.hectre.presenter.viewmodel.BaseViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 *created by Neos on 11-March-21.
 **/
abstract class BaseFragment<B : ViewBinding, VM : BaseViewModel<*>> : DaggerFragment(),
    View.OnClickListener {
    protected lateinit var binding: B
    protected lateinit var viewModel: VM

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    fun getHomeActivity() = activity as HomeActivity
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = initializeViewBinding(inflater, container)
        viewModel = viewModelFactory.create(initializeViewMode())
        initializeViews()
        return binding.root
    }

    override fun onClick(view: View) {
        view.startAnimation(
            AnimationUtils.loadAnimation(
                context,
                androidx.appcompat.R.anim.abc_fade_in
            )
        )
        clickView(view)
    }

    protected open fun clickView(view: View) {
        //do nothing
    }

    protected open fun showError(msg: String?) {
        notify(msg ?: getString(R.string.txt_sys_error), requireContext())
    }

    protected open fun showLoading() {
        ProgressLoading.instance.show(context)
    }

    protected open fun hideLoading() {
        ProgressLoading.instance.dismiss()
    }


    protected inline fun <reified T : UIState<*>> updateUI(it: T) {
        when (it.code) {
            Constants.CODE_OK -> handleSuccess(it.data, it.key!!)
            Constants.CODE_LOADING -> {
                showLoading()
                return
            }
            Constants.CODE_EXCEPTION -> showError(it.msg)
            Constants.CODE_NO_DATA -> showError(getString(R.string.txt_no_data))
            else -> showError(it.msg)
        }
        hideLoading()
    }

    protected open fun handleSuccess(data: Any?, key: Int) {
        //do nothing
    }

    abstract fun initializeViewMode(): Class<VM>
    protected abstract fun initializeViews()
    protected abstract fun initializeViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): B
}
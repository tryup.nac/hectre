package com.neos.hectre.presenter.viewmodel

import javax.inject.Inject

/**
 *created by Neos on 11-March-21.
 **/
class CommonViewModel @Inject constructor() : BaseViewModel<Nothing>()
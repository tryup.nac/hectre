package com.neos.hectre.presenter.view.fragment

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.neos.hectre.R
import com.neos.hectre.R.*
import com.neos.hectre.databinding.FragmentHomeBinding
import com.neos.hectre.presenter.view.adapter.JobsAdapter
import com.neos.hectre.presenter.viewmodel.HomeViewModel
import kotlinx.coroutines.flow.collectLatest

/**
 *created by Neos on 11-March-21.
 **/
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    companion object {
        val TAG: String = HomeFragment::class.java.name
    }

    override fun initializeViews() {
        Log.i(TAG, "initializeViews")
        lifecycleScope.launchWhenCreated {
            viewModel.dataState.collectLatest {
                if (it == null) return@collectLatest
                updateUI(it)
            }
        }
        if (viewModel.dataState.value == null) {
            lifecycleScope.launchWhenCreated {
                viewModel.listJobs()
            }
        }
        binding.actionbar.ivBack.setOnClickListener {
            getHomeActivity().finish()
        }
    }

    override fun handleSuccess(data: Any?, key: Int) {
        binding.rvStaffJob.adapter = JobsAdapter(data as ArrayList<*>, context) { key2, movie ->
            //do nothing
        }
    }

    override fun initializeViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeBinding =
        FragmentHomeBinding.inflate(inflater, container, false)

    override fun initializeViewMode(): Class<HomeViewModel> = HomeViewModel::class.java
}
package com.neos.hectre.presenter.view.activity

import com.neos.hectre.databinding.ActivityHomeBinding
import com.neos.hectre.presenter.viewmodel.CommonViewModel


class HomeActivity : BaseActivity<ActivityHomeBinding, CommonViewModel>() {
    override fun initViews() {

    }

    override fun initViewBinding(): ActivityHomeBinding {
        return ActivityHomeBinding.inflate(layoutInflater)
    }

    override fun initViewModel(): Class<CommonViewModel> {
        return CommonViewModel::class.java
    }
}
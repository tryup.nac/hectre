package com.neos.hectre.presenter.viewmodel

import androidx.lifecycle.ViewModel
import com.neos.hectre.common.Constants
import com.neos.hectre.common.Resource
import com.neos.hectre.presenter.view.fragment.screen.UIState
import kotlinx.coroutines.flow.MutableStateFlow

/**
 *created by Neos on 11-March-21.
 **/
abstract class BaseViewModel<T : UIState<*>> : ViewModel() {
    var dataState: MutableStateFlow<T?> = MutableStateFlow(null)

    companion object {
        const val KEY_COMMON = -1
    }

    inline fun <reified U, V> handleState(
        accessDataState: MutableStateFlow<U?>,
        it: Resource<V>,
        isHandleSuccess: Boolean = false,
        key: Int = KEY_COMMON
    ) {
        val clazz = Class.forName(U::class.java.name)
        val state = clazz.newInstance() as UIState<V>
        state.code = it.code
        state.msg = it.msg
        state.key = key
        if (it is Resource.Success) {
            if (it.data == null) {
                state.code = Constants.CODE_NO_DATA
                accessDataState.value = state as U
                return
            }
            state.data = it.data
            state.code = Constants.CODE_OK
            if (!isHandleSuccess) {
                handleSuccess(it, state, accessDataState, key)
            } else {
                accessDataState.value = state as U
            }
        } else {
            if (it is Resource.Loading) {
                state.code = Constants.CODE_LOADING
            } else if (it is Resource.Exception) {
                state.code = Constants.CODE_EXCEPTION
            }
            accessDataState.value = state as U
        }
    }

    open fun <V> handleSuccess(
        resource: Resource.Success<*>,
        state: UIState<*>,
        accessDataState: MutableStateFlow<V>,
        key: Int
    ) {
        accessDataState.value = state as V
    }

    @PublishedApi
    internal var accessDataState: MutableStateFlow<T?>
        get() = dataState
        set(value) {
            dataState = value
        }
}
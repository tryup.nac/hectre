package com.neos.hectre.common

import android.content.Context
import android.view.View
import android.view.animation.AnimationUtils
import com.neos.hectre.R

fun View.fadeIn(context: Context) {
    startAnimation(
        AnimationUtils.loadAnimation(
            context,
            R.anim.abc_fade_in
        )
    )
}
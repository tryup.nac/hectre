package com.neos.hectre.common

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.view.WindowManager
import android.widget.Toast
import com.neos.hectre.common.Constants.PREF_FILE
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun Context.savePref(key: String, value: String) {
    val pref: SharedPreferences =
        getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
    pref.edit().putString(key, value).apply()
}

fun Context.getPref(key: String): String? {
    val pref: SharedPreferences =
        getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
    return pref.getString(key, null)
}

fun Context.clearPref(key: String) {
    val pref: SharedPreferences =
        getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
    pref.edit().remove(key).apply()
}

fun notify(sms: String, context: Context) {
    Toast.makeText(context, sms, Toast.LENGTH_SHORT).show()
}

fun alert(
    context: Context,
    title: String = "Alert",
    msg: String,
    txtOK: String = "OK",
    txtCancel: String = "Cancel",
    event: (Constants.Event) -> Unit
) {
    val alert = AlertDialog.Builder(context).create()
    alert.setTitle(title)
    alert.setMessage(msg)
    alert.setButton(
        AlertDialog.BUTTON_POSITIVE,
        txtOK
    ) { _, _ -> event(Constants.Event.OK_BUTTON) }
    alert.setButton(
        AlertDialog.BUTTON_NEGATIVE,
        txtCancel
    ) { _, _ -> event(Constants.Event.CANCEL_BUTTON) }
    alert.show()
}


fun Dialog.doKeepDialog() {
    if (window == null) return
    val lp = WindowManager.LayoutParams()
    lp.copyFrom(window!!.attributes)
    lp.width = WindowManager.LayoutParams.MATCH_PARENT
    lp.height = WindowManager.LayoutParams.MATCH_PARENT
    window!!.attributes = lp
}

@SuppressLint("SimpleDateFormat")
fun convertDate(dateStr: String): String {
    if (dateStr.isEmpty()) return dateStr
    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val format2 = SimpleDateFormat("MMM dd, yyyy")
    try {
        val date: Date = format.parse(dateStr)
        return format2.format(date)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return dateStr
}
package com.neos.hectre.common

import com.neos.hectre.BuildConfig

/**
 *created by Neos on 11-March-21.
 **/
object Constants {
    const val   PRICE_RATE = "Price Rate"
    const val WAGES = "Wages"
    const val BASE_URL = BuildConfig.URL_SERVER_BASE
    const val HEADER_AUTHORIZATION = BuildConfig.HEADER
    const val API_KEY = BuildConfig.API_KEY

    const val TIME_OUT: Long = 30
    const val CODE_LOADING = 111
    const val CODE_OK = 200
    const val CODE_EXCEPTION = 999
    const val CODE_NO_DATA = 888
    const val PREF_FILE = "pref_saving"

    enum class Event {
        OK_BUTTON, CANCEL_BUTTON
    }

    enum class Action {
        DO_LONG_CLICK, DO_CLICK
    }
}